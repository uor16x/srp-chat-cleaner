script_name('SRP chat cleaner')
script_author('uor16x')

local SE = require 'lib.samp.events'
local player_name
local player_id

features = {
  general = {
    toggle = true,
    patterns = {},
  },
  ban = {
    toggle = true,
    patterns = {'�������������: (%S+) ������� (%S+).*%. �������: (.+)'},
  },
  demorgan = {
    toggle = true,
    patterns = {'�������������: (%S+) ������� � �������� (%S+).*%. �������: (.+)'}
  },
  warn = {
    toggle = true,
    patterns = {'�������������: (%S+) ����� warn (%S+).*%. �������: (.+)'}
  },
  ad = {
    toggle = false,
    patterns = {'����������: .+ �������: .+ ���: %d+', '�������������� ��������� News .+', '�������� �������� ��������� News .+'}
  },
  arrest = {
    toggle = false,
    patterns = {'������ (%S+) ��������� (%S+)'}
  },
  wanted = {
    toggle = false,
    patterns = {'%[Wanted (%d): (%S+)%] %[.+: (%S+)%] %[(.*)]'}
  },
  attention = {
    toggle = false,
    patterns = {'^ ��������! '}
  },
  news = {
    toggle = false,
    patterns = {'-----------=== ��������������� ������� ===-----------', '�������: %S+_%S+: %[%S+%].*'}
  },
  banchat = {
    toggle = true,
    patterns = {'(%S+) �������%(�%) ��� ���� �� �������������� (%S+).*%. �������: (.+)'}
  },
  kick = {
    toggle = true,
    patterns = {'�������������: (%S+) ������ (%S+).*%. �������: (.+)'}
  },
  namechange = {
    toggle = false,
    patterns = {'%S+ �������%(�%) ������ �� ����� ����: (%S+) >> (%S+)'}
  },
  messagesent = {
    toggle = true,
    patterns = {'^ ��������� ����������$'}
  },
  smsto = {
    toggle = true,
    patterns = {'SMS: (.*)%. ����������: (%S+_%S+)%[(%d+)%]'}
  },
  smsfrom = {
    toggle = true,
    patterns = {'SMS: (.*)%. �����������: (%S+_%S+)%[(%d+)%]'}
  },
  ask = {
    toggle = true,
    patterns = {
      '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~',
      '������� ��� ������ � ��������� ������� %- %/ask',
      '������� ������ � ������� �� ������������ ����� Samp RolePlay %- /music',
      '��� ������������ ��� ���������� �� ������ �������� �� ����� %- samp%-rp%.ru'
    }
  },
  service = {
    toggle = false,
    patterns = {
      '���������: ����� �� (%S+)%[(%d+)%]%. ��������� ���������� (%d+) �',
      '���������: ����� �� (%S+)%[(%d+)%]%. ��������� ���������� (%d+) ��',
      '(( ������� \'/service\' ����� ������� ����� ))'
    }
  },
}

function features.ban:process(text, index)
  local admin_name, user_name, reason = text:match(self.patterns[index])
  sampAddChatMessage(string.format(' {FF6347}[Ban] %s: %s', user_name, reason))
  return false
end

function features.demorgan:process(text, index)
  local admin_name, user_name, reason = text:match(self.patterns[index])
  sampAddChatMessage(string.format(' {FF6347}[DeMorgan] %s: %s', user_name, reason))
  return false
end

function features.warn:process(text, index)
  local admin_name, user_name, reason = text:match(self.patterns[index])
  sampAddChatMessage(string.format(' {FF6347}[Warn] %s: %s', user_name, reason))
  return false
end

function features.ad:process(text, index)
  return false
end

function features.arrest:process(text, index)
  local officer, suspect = text:match(self.patterns[index])
  sampAddChatMessage(string.format(' {FFB266}[Arrest] %s', suspect))
  return false
end

function features.wanted:process(text, index)
  local stars, suspect, witness, reason = text:match(self.patterns[index])
  if witness == player_name then
    sampAddChatMessage(string.format(' {66b2ff} %s{ffcc99}%s (%s)', string.rep('* ', tonumber(stars)), suspect, reason))
  end
  return false
end

function features.attention:process(text, index)
  return false
end

function features.news:process(text, index)
  return false
end

function features.banchat:process(text, index)
  local who, admin_name, reason = text:match(self.patterns[index])
  sampAddChatMessage(string.format(' {FF6347}[BanChat] %s: %s', who, reason))
  return false
end

function features.kick:process(text, index)
  print('[KICK]')
  local who, admin_name, reason = text:match(self.patterns[index])
  sampAddChatMessage(string.format(' {FF6347}[Kick] %s: %s', who, reason))
  return false
end

function features.namechange:process(text, index)
  local from, to = text:match(self.patterns[index])
  sampAddChatMessage(string.format(' {FFFF99}[NAME] %s >> %s', who, reason))
  return false
end

function features.smsfrom:process(text, index)
  local text, from, id = text:match(self.patterns[index])
  sampAddChatMessage(string.format(' {EAEB01}FROM %s [%s]: %s', from, id, text))
  return false
end

function features.smsto:process(text, index)
  local text, to, id = text:match(self.patterns[index])
  sampAddChatMessage(string.format(' {A6A600}TO %s [%s]: %s', to, id, text))
  return false
end

function features.messagesent:process(text, index)
  return false
end

function features.ask:process(name, index)
  return false
end

function features.service:process(text, index)
  if index ~= 3 then
    local who, id, range = text:match(self.patterns[index])
    if not who:find('.*Boginya_s_me4omN.*') then
      sampAddChatMessage(string.format(' {CC0000}%s[%s] - %sm.', who, id, range))
    end
  end
  return false
end

function sendInfoMsg(msg, isErr)
  local msgColor = isErr ~= nil and 0xcd4444 or 0x33ff99
  sampAddChatMessage(string.format(" [SCC] %s", msg), msgColor)
end

function main()
  if not isSampfuncsLoaded() or not isSampLoaded() then return end
	repeat wait(0) until isSampAvailable()
	wait(1500)

  _, player_id = sampGetPlayerIdByCharHandle(PLAYER_PED)
  player_name = sampGetPlayerNickname(player_id)

  sampRegisterChatCommand("scc", toggle)
  sendInfoMsg(string.format('Loaded successfully. Welcome, %s!', player_name))
end

function getToggleStatus(value)
  return value == true and '{22bb22}on' or '{bb2222}off'
end

function toggle(featureArg)
  local currFeature = (featureArg ~= nil and features[featureArg] ~= nil)
    and featureArg
    or 'general'
  features[currFeature].toggle = not features[currFeature].toggle
  sendInfoMsg(currFeature:gsub("^%l", string.upper) .. ' toggle is now ' .. getToggleStatus(features[currFeature].toggle))
end

function SE.onServerMessage(color, text)
  if not features.general.toggle then
    return true
  else
    for feature, data in pairs(features) do
      if data.toggle and #data.patterns > 0 then
        for index, pattern in ipairs(data.patterns) do
          if text:find(pattern) then
            return data:process(text, index)
          end
        end
      end
    end
  end
  return true
end